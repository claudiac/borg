package ro.upb.firerazor.srvr.io;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "sample")
public class ACFSample {
	private String body;
	private String src;
	private String dest;
	private String user;

	public ACFSample () {
		this.body = "N/A";
		this.src = "N/A";
		this.dest = "N/A";
	}
	
	public String getUser() {
		return user;
	}

	public void setUser(String user) {
		this.user = user;
	}
	
	public String getSrc() {
		return src;
	}

	public void setSrc(String src) {
		this.src = src;
	}

	public String getDest() {
		return dest;
	}

	public void setDest(String dest) {
		this.dest = dest;
	}

	public String getBody() {
		return body;
	}

	public void setBody(String body) {
		this.body = body;
	}
	
	@Override
	public boolean equals(Object other) {
		return ((ACFSample)other).getBody().equals(body);	
	}
	
	@Override 
	public int hashCode() {
		return body.hashCode();
	}
	
	@Override
	public String toString() {
		return String.format("ACFSample\nsource: '%s'\ndestinations: '%s'\nbody: '%s'\n",
				src, dest, body);
	}
}
