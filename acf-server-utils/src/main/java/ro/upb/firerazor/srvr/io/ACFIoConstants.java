package ro.upb.firerazor.srvr.io;

public class ACFIoConstants {
	public static final String CLASSIFICATION_RESULT = "classification result";
	public static final String REPUTATION_SCORE = "reputation score";
	public static final String CLASSIFIER_STATISTICS = "classifier statistics";
	public static final String ENGINE_NOT_INITIALIZED = "Engine not initialized!";
	
}
