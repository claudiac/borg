package ro.upb.firerazor.srvr.io;

import javax.xml.bind.annotation.XmlRootElement;


@XmlRootElement(name = "result")
public class ACFResponse {
	String responseType;
	double spamicity;
	double userReputation;
	double globalAccuracy;
	double specificity;
	double sensitivity;
	double relativeError;
	double precision;
	double recall;
	
	public ACFResponse() {
	}
	
	public double getGlobalAccuracy() {
		return globalAccuracy;
	}

	public void setGlobalAccuracy(double globalAccuracy) {
		this.globalAccuracy = globalAccuracy;
	}

	public double getUserReputation() {
		return userReputation;
	}

	public void setUserReputation(double userReputation) {
		this.userReputation = userReputation;
	}

	public double getSpecificity() {
		return specificity;
	}

	public void setSpecificity(double specificity) {
		this.specificity = specificity;
	}

	public double getSensitivity() {
		return sensitivity;
	}

	public void setSensitivity(double sensitivity) {
		this.sensitivity = sensitivity;
	}

	public double getRelativeError() {
		return relativeError;
	}

	public void setRelativeError(double relativeError) {
		this.relativeError = relativeError;
	}

	public double getPrecision() {
		return precision;
	}

	public void setPrecision(double precision) {
		this.precision = precision;
	}

	public double getRecall() {
		return recall;
	}

	public void setRecall(double recall) {
		this.recall = recall;
	}

	public String getResponseType() {
		return responseType;
	}
	public void setResponseType(String responseType) {
		this.responseType = responseType;
	}
	
	public double getSpamicity() {
		return spamicity;
	}
	
	public void setSpamicity(double spamicity) {
		this.spamicity = spamicity;
	}

	@Override
	public String toString() {
		if (responseType.equals(ACFIoConstants.CLASSIFICATION_RESULT)) {
			return String.format("ACFResult[%s]\nSpamicity: '%f'", responseType, spamicity);
		} else {
			if (responseType.equals(ACFIoConstants.CLASSIFIER_STATISTICS)) {
				return String.format("ACFResult[%s]\nGlobal accuracy: '%f'\nSpecificy: '%f'\nSensitivity: '%f'\nRelative error: '%f'",
				                     responseType, globalAccuracy, specificity, sensitivity, relativeError);
			} else {
				if (responseType.equals(ACFIoConstants.REPUTATION_SCORE)) {
				return String.format("ACFResult[%s]\nUser Reputation: '%f'", 
				                     responseType, userReputation);
				}
			}   
		}
		
		return "";
	}
}
