import javax.swing.*;

/**
 * Created with IntelliJ IDEA.
 * User: calinesc
 * Date: 6/30/13
 * Time: 11:23 PM
 * To change this template use File | Settings | File Templates.
 */
public class Main {
  public static void main(String args[]) {
    JFrame frame = new JFrame("Demo");
    Controller clientLogic = new Controller();
    frame.setContentPane(new GUI(clientLogic).$$$getRootComponent$$$());
    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    frame.pack();
    frame.setLocationRelativeTo(null);
    frame.setVisible(true);
  }
}
