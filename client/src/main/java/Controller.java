import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import ro.upb.firerazor.data.impl.FileToACFSampleConveter;
import ro.upb.firerazor.srvr.io.ACFIoConstants;
import ro.upb.firerazor.srvr.io.ACFResponse;
import ro.upb.firerazor.srvr.io.ACFSample;

import java.io.File;

/**
 * Created with IntelliJ IDEA.
 * User: calinesc
 * Date: 6/30/13
 * Time: 11:17 PM
 * To change this template use File | Settings | File Templates.
 */
public class Controller {
  private WebResource wsServer;

  public Controller() {
    Client client = Client.create();
    wsServer = client.resource("http://localhost:8080/acf-server");
  }
  public String makeStatsRequest() {
    WebResource webResource = wsServer.path("/stats");
    ClientResponse response = webResource.type("application/xml").get(ClientResponse.class);

    if (response.getStatus() != 200) {
      throw new RuntimeException("Failed : HTTP error code : "
          + response.getStatus());
    }

    ACFResponse output = response.getEntity(ACFResponse.class);

    if (output.getResponseType().equals(ACFIoConstants.ENGINE_NOT_INITIALIZED)) {
      return output.getResponseType();
    }

    return output.toString();

  }

  public String makeUserRequest(String userId) {
    WebResource webResource = wsServer.path("/user");

    ACFSample request = new ACFSample();
    request.setUser(userId);

    ClientResponse response = webResource.type("application/xml")
        .put(ClientResponse.class, request);

    if (response.getStatus() != 200) {
      throw new RuntimeException("Failed : HTTP error code : "
          + response.getStatus());
    }

    ACFResponse output = response.getEntity(ACFResponse.class);

    if (output.getResponseType().equals(ACFIoConstants.ENGINE_NOT_INITIALIZED)) {
      return output.getResponseType();
    }

    return String.format("The specified user [%s] has a %f\nprobability of sending a spam message.", userId,
        output.getUserReputation());
  }

  public String makeClassifyRequest(File input) {
    WebResource webResource = wsServer.path("/classify");

    ClientResponse response = webResource.type("application/xml")
        .put(ClientResponse.class, new FileToACFSampleConveter().convert(input));

    if (response.getStatus() != 200) {
      throw new RuntimeException("Failed : HTTP error code : "
          + response.getStatus());
    }

    ACFResponse output = response.getEntity(ACFResponse.class);

    if (output.getResponseType().equals(ACFIoConstants.ENGINE_NOT_INITIALIZED)) {
      return output.getResponseType();
    }

    return String.format("The specified file [%s]\nhas a spamicity probability of %f", input.getName(),
        output.getSpamicity());

  }
}
