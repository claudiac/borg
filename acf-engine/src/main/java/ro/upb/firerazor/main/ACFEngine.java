package ro.upb.firerazor.main;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import ro.upb.firerazor.liblintomltsvm.SVMClassifierTrainer;
import ro.upb.firerazor.srvr.io.ACFIoConstants;
import ro.upb.firerazor.srvr.io.ACFResponse;
import ro.upb.firerazor.srvr.io.ACFSample;
import ro.upb.firerazor.units.Plugin;
import ro.upb.firerazor.units.SuperClassifier;
import ro.upb.firerazor.units.impl.LibLinSGDSuperClassfier;
import ro.upb.firerazor.units.impl.StackedMalletClassifiers;
import ro.upb.firerazor.units.impl.UserComputedScorePlugin;
import ro.upb.firerazor.utils.ACFEngineConstants;
import cc.mallet.classify.DecisionTreeTrainer;
import cc.mallet.classify.NaiveBayesTrainer;

public class ACFEngine {
  private final SuperClassifier classifier;
  private final List<Plugin> PLUGINS = Arrays.asList(new StackedMalletClassifiers(Arrays.asList(
      SVMClassifierTrainer.class, NaiveBayesTrainer.class, DecisionTreeTrainer.class)),
      new UserComputedScorePlugin());
  private boolean isInitialized;

  public ACFEngine(String corpus) {
    classifier = new LibLinSGDSuperClassfier();
    classifier.addPlugins(PLUGINS);
    classifier.setCorpusLocation(new File(corpus));
    isInitialized = false;
  }

  public void loadDefaultConfiguration() throws FileNotFoundException {
    classifier.initFromDefaultLocation();
    isInitialized = true;
  }

  public void train() {
    classifier.train();
    isInitialized = true;
    classifier.evaluate();
  }

  public boolean isInitialized() {
    return isInitialized;
  }

  public ACFResponse classify(final ACFSample request) {
    final ACFResponse response = classifier.getClassificationResult(request);
    Thread savefile = new Thread() {
      @Override
      public void run() {
        try {
          BufferedWriter outWriter;
          if (response.getSpamicity() >= 0.8) {
            outWriter = new BufferedWriter(new FileWriter(new File(new File(new File(classifier
                .getCorpusLocation(), ACFEngineConstants.TRAINING_FOLDER),
                ACFEngineConstants.SPAM_FOLDER), "sample_" + System.currentTimeMillis())));
          } else {
            if (response.getSpamicity() <= 0.2) {
              outWriter = new BufferedWriter(new FileWriter(new File(new File(new File(classifier
                  .getCorpusLocation(), ACFEngineConstants.TRAINING_FOLDER),
                  ACFEngineConstants.HAM_FOLDER), "sample_" + System.currentTimeMillis())));
            } else {
              return;
            }
          }

          outWriter.write(String.format("From:%s\nTo:%s\n%s", request.getSrc(), request.getDest(),
              request.getBody()));
          outWriter.close();
        } catch (IOException e) {
          e.printStackTrace();
        }
      }
    };
    savefile.start();
    return classifier.getClassificationResult(request);
  }

  public ACFResponse getStats() {
    return classifier.getStats();

  }

  public void writeDefaultConfiguration() {
    classifier.writeClassifierToDefaultLocation();
  }

  public ACFResponse getUserReputation(ACFSample request) {
    ACFResponse response = new ACFResponse();
    response.setResponseType(ACFIoConstants.ENGINE_NOT_INITIALIZED);
    for (Plugin plugin : classifier.getPlugins()) {
      if (plugin instanceof UserComputedScorePlugin) {
        response.setResponseType(ACFIoConstants.REPUTATION_SCORE);
        response.setUserReputation(((UserComputedScorePlugin) plugin).getUserScore(request));
      }
    }
    return response;
  }
}
