package ro.upb.firerazor.tests;

import java.io.File;

import ro.upb.firerazor.data.impl.FileToACFSampleConveter;
import ro.upb.firerazor.units.impl.UserComputedScorePlugin;
import ro.upb.firerazor.units.utils.ClassifierAnalyzer;
import ro.upb.firerazor.units.utils.ScoreVector;
import ro.upb.firerazor.utils.ACFEngineConstants;

public class UserComputedScorePluginTest {
  public static void main(String[] args) {
    UserComputedScorePlugin plugin = new UserComputedScorePlugin();
    plugin.trainPlugin(new File("sacorpus/training"));

    ClassifierAnalyzer analyzer = new ClassifierAnalyzer();

    FileToACFSampleConveter converter = new FileToACFSampleConveter();

    for (File category : new File("sacorpus/testing").listFiles()) {
      if (!ACFEngineConstants.categories.contains(category.getName())) {
        continue;
      }
      for (File sample : category.listFiles()) {
        ScoreVector score = plugin.getScore(converter.convert(sample));
        String label = "spam";

        if (score.getScore("score") < 0.5) {
          label = "ham";
        }
        analyzer.recordAnswer(category.getName(), label, score.getScore("score"));
      }
    }

    System.out.println(analyzer.getStats());
  }

}
