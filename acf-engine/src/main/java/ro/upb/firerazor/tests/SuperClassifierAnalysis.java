package ro.upb.firerazor.tests;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import ro.upb.firerazor.liblintomltsvm.SVMClassifierTrainer;
import ro.upb.firerazor.units.Plugin;
import ro.upb.firerazor.units.impl.LibLinSGDSuperClassfier;
import ro.upb.firerazor.units.impl.StackedMalletClassifiers;
import ro.upb.firerazor.units.impl.UserComputedScorePlugin;
import ro.upb.firerazor.units.utils.ClassifierStats;
import ro.upb.firerazor.utils.ACFEngineConstants;
import cc.mallet.classify.DecisionTreeTrainer;
import cc.mallet.classify.NaiveBayesTrainer;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

public class SuperClassifierAnalysis {
  static Map<String, List<? extends Plugin>> scenarios = Maps.newHashMap();

  static {
    // single plugin
    scenarios.put("User Score", Arrays.asList(new UserComputedScorePlugin()));
    scenarios.put("SVM", Arrays.asList(new StackedMalletClassifiers(Arrays
        .asList(SVMClassifierTrainer.class))));
    scenarios.put("Naive Bayes", Arrays.asList(new StackedMalletClassifiers(Arrays
        .asList(NaiveBayesTrainer.class))));
    scenarios.put("Decision Tree", Arrays.asList(new StackedMalletClassifiers(Arrays
        .asList(DecisionTreeTrainer.class))));

    // two plugins
    scenarios.put("SVM, User Score", Arrays.asList(new UserComputedScorePlugin(),
        new StackedMalletClassifiers(Arrays.asList(SVMClassifierTrainer.class))));
    scenarios.put("Decision Tree, SVM", Arrays.asList(new StackedMalletClassifiers(Arrays.asList(
        DecisionTreeTrainer.class, SVMClassifierTrainer.class))));

    scenarios.put("Naive Bayes, User Score", Arrays.asList(new UserComputedScorePlugin(),
        new StackedMalletClassifiers(Arrays.asList(NaiveBayesTrainer.class))));
    scenarios.put("Naive Bayes, SVM", Arrays.asList(new StackedMalletClassifiers(Arrays.asList(
        SVMClassifierTrainer.class, NaiveBayesTrainer.class))));

    scenarios.put("Decision Tree, User Score", Arrays.asList(new UserComputedScorePlugin(),
        new StackedMalletClassifiers(Arrays.asList(DecisionTreeTrainer.class))));
    scenarios.put("Decision Tree, Naive Bayes", Arrays.asList(new StackedMalletClassifiers(Arrays
        .asList(NaiveBayesTrainer.class, DecisionTreeTrainer.class))));

    // three plugins
    scenarios.put("Decision Tree, SVM, User Score", Arrays.asList(new UserComputedScorePlugin(),
        new StackedMalletClassifiers(Arrays.asList(SVMClassifierTrainer.class,
            DecisionTreeTrainer.class))));
    scenarios.put("Naive Bayes, SVM, User Score", Arrays.asList(new UserComputedScorePlugin(),
        new StackedMalletClassifiers(Arrays.asList(SVMClassifierTrainer.class,
            NaiveBayesTrainer.class))));

    scenarios.put("Decision Tree, Naive Bayes, SVM", Arrays.asList(new StackedMalletClassifiers(
        Arrays.asList(DecisionTreeTrainer.class, NaiveBayesTrainer.class,
            SVMClassifierTrainer.class))));

    // all plugins
    scenarios.put("Decision Tree, Naive Bayes, SVM, User Score", Arrays.asList(
        new StackedMalletClassifiers(Arrays.asList(DecisionTreeTrainer.class,
            NaiveBayesTrainer.class, SVMClassifierTrainer.class)), new UserComputedScorePlugin()));
  }

  static List<String> corpuses = Lists.newArrayList();
  static {
    // corpuses.add("sacorpus");
    corpuses.add("csdmcorpus");
    // corpuses.add("enroncorpus");
    // corpuses.add("allcorpus");
  }

  static class ScenarioInformation {
    ClassifierStats stats;
    double duration;
    String name;

    public ScenarioInformation(ClassifierStats stats, double duration, String name) {
      this.stats = stats;
      this.duration = duration;
      this.name = name;
    }

    public String getName() {
      return name;
    }

    public void setName(String name) {
      this.name = name;
    }

    public ClassifierStats getStats() {
      return stats;
    }

    public void setStats(ClassifierStats stats) {
      this.stats = stats;
    }

    public double getDuration() {
      return duration;
    }

    public void setDuration(double duration) {
      this.duration = duration;
    }

  }

  static int getTrainSamples(String directory) {
    File trainDir = new File(directory, ACFEngineConstants.TRAINING_FOLDER);
    int count = 0;

    for (File category : trainDir.listFiles()) {
      if (ACFEngineConstants.categories.contains(category.getName())) {
        for (File sample : category.listFiles()) {
          if (sample.isFile()) {
            ++count;
          }
        }
      }
    }

    return count;
  }

  public static void main(String[] args) throws IOException {
    BufferedWriter out = new BufferedWriter(new FileWriter("stats.txt"));

    for (String corpus : corpuses) {
      List<ScenarioInformation> globalInfos = Lists.newArrayList();
      int totalTrainSamples = getTrainSamples(corpus);
      System.out.println(String.format("Testing with corpus '%s' [total: %d] ...", corpus,
          totalTrainSamples));
      out.write(String.format("Testing with corpus '%s' [total: %d] ...\n", corpus,
          totalTrainSamples));
      out.flush();
      for (Map.Entry<String, List<? extends Plugin>> entry : scenarios.entrySet()) {
        System.out.println(String.format("Scenario: '%s'", entry.getKey()));
        out.write(String.format("Scenario: '%s'\n", entry.getKey()));
        out.flush();
        for (int i = 1; i < 11; ++i) {
          LibLinSGDSuperClassfier trainer = new LibLinSGDSuperClassfier();
          trainer.setCorpusLocation(new File(corpus));
          trainer.addPlugins(entry.getValue());
          double start_time = System.currentTimeMillis();
          trainer.train(i / 10.0);
          double end_time = System.currentTimeMillis();
          ClassifierStats stats = trainer.evaluate();

          if (i == 10) {
            globalInfos.add(new ScenarioInformation(stats, end_time - start_time, entry.getKey()));
          }
          System.out.println(String.format("%d %f %f %s", (int) (totalTrainSamples * (i / 10.0)),
              end_time - start_time, stats.getRelativeError(), stats
                  .getConfusionMatrixRepresentation()));

          out.write(String.format("%d %f %f %s\n", (int) (totalTrainSamples * (i / 10.0)), end_time
              - start_time, stats.getRelativeError(), stats.getConfusionMatrixRepresentation()));
          out.flush();
        }
      }

      System.out.println("Stats for corpus:");
      for (ScenarioInformation info : globalInfos) {
        System.out.println(String.format("'%s' %f %f %s", info.getName(), info.getDuration(), info
            .getStats().getRelativeError(), info.getStats().getConfusionMatrixRepresentation()));
        out.write(String.format("'%s' %f %f %s", info.getName(), info.getDuration(), info
            .getStats().getRelativeError(), info.getStats().getConfusionMatrixRepresentation()));
        out.flush();
      }
    }

    out.close();
  }
}
