package ro.upb.firerazor.tests;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import ro.upb.firerazor.srvr.io.ACFSample;
import ro.upb.firerazor.tests.SuperClassifierAnalysis.ScenarioInformation;
import ro.upb.firerazor.units.Plugin;
import ro.upb.firerazor.units.impl.LibLinSGDSuperClassfier;
import ro.upb.firerazor.units.utils.ClassifierStats;
import ro.upb.firerazor.utils.ACFEngineUtils;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Collections;
import java.util.List;
import java.util.Map;

public class SuperClassifierTrainScenario {

  public static void main(String[] args) throws IOException {
    String corpus = args[0];
    BufferedWriter out = new BufferedWriter(new FileWriter(String.format("timestats_%s_%s.txt",
        args[0], args[1])));

    /* Get corpus files */
    List<File> allFiles = ACFEngineUtils.getFilesFromPhaseDir(new File(corpus));

    /* Split training and testing files */
    Collections.shuffle(allFiles);
    List<List<File>> allFilesPart = Lists.partition(allFiles, (int) (allFiles.size() * 0.8));
    List<File> trainFiles = allFilesPart.get(0);
    List<File> testFiles = allFilesPart.get(1);

    Map<ACFSample, String> testSamples = ACFEngineUtils.getACFSamplesFromFiles(testFiles);

    System.out.println(String.format("Testing with corpus '%s' [total: %d] ...", corpus, allFiles
        .size()));
    out.write(String.format("Testing with corpus '%s' [total: %d] ...\n", corpus, allFiles.size()));
    out.flush();
    Collections.shuffle(trainFiles);

    for (Map.Entry<String, List<? extends Plugin>> entry : SuperClassifierAnalysis.scenarios
        .entrySet()) {
      System.out.println(String.format("Scenario: '%s'", entry.getKey()));
      out.write(String.format("Scenario: '%s'\n", entry.getKey()));
      out.flush();

      List<File> trainSlice = Lists.newArrayList();
      Map<ACFSample, String> trainSample = Maps.newHashMap();
      for (int i = 0; i < trainSplits.size(); ++i) {
        LibLinSGDSuperClassfier trainer = new LibLinSGDSuperClassfier();
        trainer.setCorpusLocation(new File(corpus));
        trainer.addPlugins(entry.getValue());
        trainSlice.addAll(trainSplits.get(i));
        trainSample.putAll(ACFEngineUtils.getACFSamplesFromFiles(trainSplits.get(i)));
        double start_time = System.currentTimeMillis();
        // trainer.train(0.2);
        trainer.trainSuper(trainSlice);
        double end_time = System.currentTimeMillis();
        ClassifierStats statsTest = trainer.evaluateSuper(testSamples, 0.5);
        // ClassifierStats statsTrain = trainer.evaluateSuper(trainSample, 0.5);

        System.out.println(String.format("%d %f %f %s", trainSlice.size(), end_time - start_time,
            statsTest.getRelativeError(), statsTest.getConfusionMatrixRepresentation()));
        out.write(String.format("%d %f %f %s\n", trainSlice.size(), end_time - start_time,
            statsTest.getRelativeError(), statsTest.getConfusionMatrixRepresentation()));
        out.flush();

        if (i == (trainSplits.size() - 1)) {
          out.write("\n");
          globalInfos
              .add(new ScenarioInformation(statsTest, end_time - start_time, entry.getKey()));
          for (int j = 1; j < 10; ++j) {
            double threshold = j / 10.0;
            ClassifierStats rstats = trainer.evaluateSuper(testSamples, threshold);
            // ClassifierStats tstats = trainer.evaluateSuper(trainSample,
            // threshold);
            System.out.println(String.format("%s", rstats.getConfusionMatrixRepresentation()));
            out.write(String.format("%s\n", rstats.getConfusionMatrixRepresentation()));
            out.flush();
          }
          out.write("\n");
        }
      }
    }

    for (ScenarioInformation info : globalInfos) {
      System.out.println(String.format("'%s' %f %f %s", info.getName(), info.getDuration(),
          info.stats.getRelativeError(), info.stats.getConfusionMatrixRepresentation()));
      out.write(String.format("'%s' %f %f %s\n", info.getName(), info.getDuration(), info.stats
          .getRelativeError(), info.stats.getConfusionMatrixRepresentation()));
      out.flush();
    }
    out.write("\n");
    out.close();
  }
}
