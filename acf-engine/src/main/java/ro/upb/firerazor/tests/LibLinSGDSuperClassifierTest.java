package ro.upb.firerazor.tests;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Arrays;

import ro.upb.firerazor.liblintomltsvm.SVMClassifierTrainer;
import ro.upb.firerazor.units.impl.LibLinSGDSuperClassfier;
import ro.upb.firerazor.units.impl.StackedMalletClassifiers;
import ro.upb.firerazor.units.impl.UserComputedScorePlugin;
import cc.mallet.classify.DecisionTreeTrainer;
import cc.mallet.classify.NaiveBayesTrainer;

public class LibLinSGDSuperClassifierTest {
  public static void main(String[] args) {
    LibLinSGDSuperClassfier trainer = new LibLinSGDSuperClassfier();
    trainer.setCorpusLocation(new File(args[0]));
    trainer.addPlugin(new StackedMalletClassifiers(Arrays.asList(NaiveBayesTrainer.class,
        DecisionTreeTrainer.class, SVMClassifierTrainer.class)));
    // trainer.addPlugin(new
    // StackedMalletClassifiers(Arrays.asList(SVMClassifierTrainer.class)));
    trainer.addPlugin(new UserComputedScorePlugin());
    // double crt_time = System.currentTimeMillis();
    trainer.train();
    trainer.writeClassifierToDefaultLocation();
    try {
      trainer.initFromDefaultLocation();
    } catch (FileNotFoundException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }
    trainer.evaluate();
    System.out.println(trainer.getStats());
    // ACFSample sample = new ACFSample();
    // sample.setBody("you have a valid point");
    // sample.setDest("h@c.com");
    // sample.setSrc("f@g.com");
    // System.out.println(trainer.getStats());
    // System.out.println(trainer.classify(sample));
    // System.out.println(String.format("%f minutes",
    // (System.currentTimeMillis() - crt_time) / 1000 / 60));
  }
}