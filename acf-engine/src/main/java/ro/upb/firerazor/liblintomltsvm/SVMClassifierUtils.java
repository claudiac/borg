package ro.upb.firerazor.liblintomltsvm;

import de.bwaldvogel.liblinear.Feature;
import de.bwaldvogel.liblinear.Linear;
import de.bwaldvogel.liblinear.Model;
import de.bwaldvogel.liblinear.Parameter;
import de.bwaldvogel.liblinear.Problem;
import de.bwaldvogel.liblinear.SolverType;

public class SVMClassifierUtils {
  public static Model trainGenericModel(int numFeatures, int numSamples, Feature[][] x, double[] y) {
    Problem problem = new Problem();
    problem.l = numSamples;
    problem.n = numFeatures;

    problem.x = x;
    problem.y = y;

    SolverType solver = SolverType.L2R_L2LOSS_SVR_DUAL;
    double C = 1.0;
    double eps = 0.03;

    Parameter parameter = new Parameter(solver, C, eps);
    return Linear.train(problem, parameter);
  }
}
