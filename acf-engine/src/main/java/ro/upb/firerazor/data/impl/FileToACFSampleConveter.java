package ro.upb.firerazor.data.impl;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ro.upb.firerazor.data.DataConverter;
import ro.upb.firerazor.srvr.io.ACFSample;

public class FileToACFSampleConveter implements DataConverter<File, ACFSample> {
  private static final Logger logger = LoggerFactory.getLogger(FileToACFSampleConveter.class);
  private final String MAIL_REGEX = "[A-Za-z0-9_.]+@[A-Za-z0-9_.]+";

  @Override
  public ACFSample convert(File input) {

    BufferedReader fReader;
    ACFSample sample = new ACFSample();
    StringBuffer content = new StringBuffer();
    try {
      fReader = new BufferedReader(new FileReader(input));
      try {
        while (fReader.ready()) {
          String line = fReader.readLine();
          if (line.startsWith("To:")) {
            sample.setDest(getEmails(line));
          } else {
            if (line.startsWith("From:")) {
              sample.setSrc(getEmails(line));
            }
          }
          content.append(line);
        }

        fReader.close();

        sample.setBody(content.toString());

        return sample;
      } catch (IOException e) {
        logger.warn(String.format("An error occured while reading the file [%s]", input
            .getAbsolutePath()), e);
        return null;
      }
    } catch (FileNotFoundException e) {
      logger.warn(String.format("File [%s] was not found", input.getAbsolutePath()), e);
      return null;
    }

  }

  private String getEmails(String line) {
    Pattern emailPattern = Pattern.compile(MAIL_REGEX);
    Matcher matcher = emailPattern.matcher(line);
    StringBuffer mails = new StringBuffer();

    boolean match = matcher.find();
    while (match) {
      mails.append(matcher.group());
      match = matcher.find();
      if (match) {
        mails.append(",");
      }
    }

    return mails.toString();
  }

}
