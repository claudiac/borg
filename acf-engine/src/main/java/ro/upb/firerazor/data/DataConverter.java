package ro.upb.firerazor.data;

public interface DataConverter<F, T> {
    public T convert(F input);
}
