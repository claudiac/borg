package ro.upb.firerazor.data.impl;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

import ro.upb.firerazor.data.DataConverter;
import cc.mallet.pipe.CharSequence2TokenSequence;
import cc.mallet.pipe.FeatureSequence2FeatureVector;
import cc.mallet.pipe.Input2CharSequence;
import cc.mallet.pipe.Pipe;
import cc.mallet.pipe.SerialPipes;
import cc.mallet.pipe.Target2Label;
import cc.mallet.pipe.TokenSequence2FeatureSequence;
import cc.mallet.pipe.TokenSequenceLowercase;
import cc.mallet.pipe.TokenSequenceRemoveStopwords;
import cc.mallet.pipe.iterator.FileIterator;
import cc.mallet.pipe.iterator.FileListIterator;
import cc.mallet.types.InstanceList;

public class FolderToMalletInstanceListConvertor implements DataConverter<File, InstanceList> {
  public Pipe buildPipe() {
    ArrayList<Pipe> pipeList = new ArrayList<Pipe>();

    // Read data from File objects
    pipeList.add(new Input2CharSequence("UTF-8"));

    // Regular expression for what constitutes a token.
    Pattern tokenPattern = Pattern.compile("[\\p{L}\\p{N}_]+");

    // Tokenize raw strings
    pipeList.add(new CharSequence2TokenSequence(tokenPattern));

    // Normalize all tokens to all lowercase
    pipeList.add(new TokenSequenceLowercase());

    // Remove stopwords from a standard English stoplist.
    pipeList.add(new TokenSequenceRemoveStopwords(false, false));

    // Rather than storing tokens as strings, convert
    // them to integers by looking them up in an alphabet.
    pipeList.add(new TokenSequence2FeatureSequence());

    // Do the same thing for the "target" field:
    // convert a class label string to a Label object,
    // which has an index in a Label alphabet.
    pipeList.add(new Target2Label());

    // Now convert the sequence of features to a sparse vector,
    // mapping feature IDs to counts.
    pipeList.add(new FeatureSequence2FeatureVector());

    return new SerialPipes(pipeList);
  }

  @Override
  public InstanceList convert(File input) {
    Pipe pipeline = buildPipe();

    FileIterator iterator = new FileIterator(new File[] { input }, FileIterator.LAST_DIRECTORY);

    InstanceList instances = new InstanceList(pipeline);
    instances.addThruPipe(iterator);

    return instances;
  }

  public InstanceList convert(List<File> input) {
    Pipe pipe = buildPipe();
    InstanceList instances = new InstanceList(pipe);

    FileIterator iterator = new FileIterator(input.toArray(new File[input.size()]),
        FileIterator.LAST_DIRECTORY);
    instances.addThruPipe(iterator);

    return instances;
  }

  public InstanceList convertFiles(List<File> input) {
    Pipe pipe = buildPipe();
    InstanceList instances = new InstanceList(pipe);

    FileListIterator iterator = new FileListIterator(input.toArray(new File[input.size()]), null,
        FileListIterator.STARTING_DIRECTORIES, true);
    instances.addThruPipe(iterator);

    return instances;
  }

}
