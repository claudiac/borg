package ro.upb.firerazor.units.utils;

import java.io.Serializable;

public class ClassifierStats implements Serializable {
  private Double accuracy;
  private Double relativeError;
  private Double sensitivity;
  private Double specificity;
  private Double recall;
  private Double precision;

  private Integer[][] confusionMatrix;

  public Double getAccuracy() {
    return accuracy;
  }

  public void setAccuracy(Double accuracy) {
    this.accuracy = accuracy;
  }

  public Double getRelativeError() {
    return relativeError;
  }

  public void setRelativeError(Double relativeError) {
    this.relativeError = relativeError;
  }

  public Double getSensitivity() {
    return sensitivity;
  }

  public void setSensitivity(Double sensitivity) {
    this.sensitivity = sensitivity;
  }

  public Double getSpecificity() {
    return specificity;
  }

  public void setSpecificity(Double specificity) {
    this.specificity = specificity;
  }

  public Double getRecall() {
    return recall;
  }

  public void setRecall(Double recall) {
    this.recall = recall;
  }

  public Double getPrecision() {
    return precision;
  }

  public void setPrecision(Double precision) {
    this.precision = precision;
  }

  public Integer[][] getConfusionMatrix() {
    return confusionMatrix;
  }

  public void setConfusionMatrix(Integer[][] confusion_matrix) {
    this.confusionMatrix = confusion_matrix;
  }

  public String getConfusionMatrixRepresentation() {
    if (confusionMatrix != null) {
      return String.format("%d %d %d %d",
          confusionMatrix[ClassifierAnalyzer.PRED_HAM][ClassifierAnalyzer.REAL_HAM],
          confusionMatrix[ClassifierAnalyzer.PRED_HAM][ClassifierAnalyzer.REAL_SPAM],
          confusionMatrix[ClassifierAnalyzer.PRED_SPAM][ClassifierAnalyzer.REAL_HAM],
          confusionMatrix[ClassifierAnalyzer.PRED_SPAM][ClassifierAnalyzer.REAL_SPAM]);
    }

    return "";
  }

  @Override
  public String toString() {
    StringBuffer result = new StringBuffer();

    result.append(String.format("Global accuracy: %f\n", accuracy.doubleValue()));
    result.append(String.format("Sensitivity: %f\n", sensitivity.doubleValue()));
    result.append(String.format("Specificy: %f\n", specificity.doubleValue()));
    result.append(String.format("Precision: %f\n", precision.doubleValue()));
    result.append(String.format("Recall: %f\n", recall.doubleValue()));
    result.append(String.format("Relative errror: %f\n", relativeError.doubleValue()));

    return result.toString();
  }
}
