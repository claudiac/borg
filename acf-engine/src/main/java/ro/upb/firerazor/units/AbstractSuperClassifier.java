package ro.upb.firerazor.units;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import ro.upb.firerazor.data.impl.FileToACFSampleConveter;
import ro.upb.firerazor.srvr.io.ACFIoConstants;
import ro.upb.firerazor.srvr.io.ACFResponse;
import ro.upb.firerazor.srvr.io.ACFSample;
import ro.upb.firerazor.units.utils.ClassifierAnalyzer;
import ro.upb.firerazor.units.utils.ClassifierStats;
import ro.upb.firerazor.units.utils.ScoreVector;
import ro.upb.firerazor.utils.ACFEngineConstants;
import ro.upb.firerazor.utils.ACFEngineUtils;

import com.google.common.collect.Lists;

public abstract class AbstractSuperClassifier extends AbstractClassifier implements SuperClassifier {
  protected final List<Plugin> plugins = Lists.newArrayList();
  private final Lock corpusLock = new ReentrantLock();
  protected File corpusDir;

  @Override
  public void setCorpusLocation(File corpus) {
    corpusLock.lock();
    this.corpusDir = corpus;
    corpusLock.unlock();
  }

  @Override
  public void train() {
    if (corpusDir == null) {
      logError("The corpus directory was not initialized!", new UninitializedTrainerException());
      return;
    }

    corpusLock.lock();
    trainBatch(new File(corpusDir, ACFEngineConstants.TRAINING_FOLDER));
    corpusLock.unlock();
  }

  @Override
  public void trainSuper(List<File> trainFiles) {
    System.out.println("Train super");

    corpusLock.lock();
    getSuperModel(trainFiles);
    corpusLock.unlock();

    writeConfLock.lock();
    updateClassifier();
    writeConfLock.unlock();

    isTrained = true;
  }

  protected abstract void getSuperModel(List<File> trainFiles);

  @Override
  public File getCorpusLocation() {
    return corpusDir;
  }

  @Override
  public void train(double percent) {
    if (corpusDir == null) {
      logError("The corpus directory was not initialized!", new UninitializedTrainerException());
      return;
    }

    corpusLock.lock();
    trainBatch(new File(corpusDir, ACFEngineConstants.TRAINING_FOLDER), percent);
    corpusLock.unlock();
  }

  @Override
  public ClassifierStats evaluateSuper(Map<ACFSample, String> testSamples, double threshold) {
    ClassifierAnalyzer analyzer = new ClassifierAnalyzer();

    corpusLock.lock();
    for (Entry<ACFSample, String> entry : testSamples.entrySet()) {
      double score = classify(entry.getKey()).getScore(ACFEngineConstants.SPAM_FOLDER);
      String predicted = ACFEngineConstants.SPAM_FOLDER;
      if (score < threshold) {
        score = 1 - score;
        predicted = ACFEngineConstants.HAM_FOLDER;
      }
      analyzer.recordAnswer(entry.getValue(), predicted, score);
    }

    corpusLock.unlock();
    return analyzer.getStats();
  }

  @Override
  protected void trainModel(List<File> trainDir) {
    //
  }

  @Override
  protected void trainModel(File trainDir) {
    if (plugins.size() == 0) {
      logError("This trainer has no plugins to train on!", new UninitializedTrainerException());
      return;
    }

    getSuperModel(trainDir);
  }

  @Override
  protected void trainModel(File trainDir, double percent) {
    if (plugins.size() == 0) {
      logError("This trainer has no plugins to train on!", new UninitializedTrainerException());
      return;
    }

    System.out.println("La train model cu percent");
    getSuperModel(trainDir, percent);
  }

  @Override
  public void addPlugin(Plugin plugin) {
    plugins.add(plugin);
  }

  @Override
  public void addPlugins(List<? extends Plugin> list) {
    this.plugins.addAll(list);
  }

  @Override
  public List<Plugin> getPlugins() {
    return plugins;
  }

  @Override
  public ClassifierStats evaluate() {
    if (corpusDir == null) {
      logError("The corpus directory was not initialized!", new UninitializedTrainerException());
      return null;
    }

    ClassifierStats result;
    corpusLock.lock();
    result = evaluate(new File(corpusDir, ACFEngineConstants.TESTING_FOLDER));
    corpusLock.unlock();

    ACFEngineUtils.writeObjectToFile(result, new File(ACFEngineUtils.getEvaluationFileForClass(this
        .getClass())));

    return result;
  }

  @Override
  protected ClassifierStats evaluateClassifier(File testDir) {
    ClassifierAnalyzer analyzer = new ClassifierAnalyzer();

    FileToACFSampleConveter converter = new FileToACFSampleConveter();

    for (File category : testDir.listFiles()) {
      if (!ACFEngineConstants.categories.contains(category.getName())) {
        continue;
      }
      for (File sample : category.listFiles()) {
        ScoreVector score = classify(converter.convert(sample));
        analyzer.recordAnswer(category.getName(), score.getBestLabel(), score.getScore(score
            .getBestLabel()));
      }
    }

    return analyzer.getStats();
  }

  @Override
  public void initFromDefaultLocation() throws FileNotFoundException {
    initFromFile(defaultModelLocation);
  }

  @Override
  public void initFromFile(File inputModel) throws FileNotFoundException {
    for (Plugin plugin : plugins) {
      plugin.readConfigurationFromDefaultLocation();
    }

    super.initFromFile(inputModel);
  }

  @Override
  public void writeClassifierToFile(File outputModel) {
    if (!isTrained) {
      logError("Classifier was not initialized!", new UninitializedClassifierException());
    }

    for (Plugin plugin : plugins) {
      plugin.writeConfigurationToDefaultLocation();
    }

    super.writeClassifierToFile(outputModel);
  }

  @Override
  public void writeClassifierToDefaultLocation() {
    writeClassifierToFile(defaultModelLocation);
  }

  @Override
  public ACFResponse getClassificationResult(ACFSample sample) {
    ACFResponse response = new ACFResponse();

    response.setResponseType(ACFIoConstants.CLASSIFICATION_RESULT);
    response.setSpamicity(classify(sample).getScore(ACFEngineConstants.SPAM_FOLDER));

    return response;
  }

  @Override
  public ACFResponse getStats() {
    ACFResponse response = new ACFResponse();
    File evalFile = new File(ACFEngineUtils.getEvaluationFileForClass(this.getClass()));
    if (!evalFile.exists()) {
      evaluate();
    }
    try {
      ClassifierStats stats = (ClassifierStats) ACFEngineUtils.readObjectFromFile(evalFile);

      response.setResponseType(ACFIoConstants.CLASSIFIER_STATISTICS);
      response.setGlobalAccuracy(stats.getAccuracy());
      response.setPrecision(stats.getPrecision());
      response.setRecall(stats.getRecall());
      response.setRelativeError(stats.getRelativeError());
      response.setSensitivity(stats.getSensitivity());
      response.setSpecificity(stats.getSpecificity());

    } catch (FileNotFoundException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }

    return response;
  }

  protected abstract void getSuperModel(File trainDir);

  protected abstract void getSuperModel(File trainDir, double percent);

  @SuppressWarnings("serial")
  public static class UninitializedTrainerException extends Exception {
    public UninitializedTrainerException() {
      super();
    }
  }
}
