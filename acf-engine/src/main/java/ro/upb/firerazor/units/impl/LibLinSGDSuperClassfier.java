package ro.upb.firerazor.units.impl;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map.Entry;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ro.upb.firerazor.data.DataConverter;
import ro.upb.firerazor.data.impl.FileToACFSampleConveter;
import ro.upb.firerazor.srvr.io.ACFSample;
import ro.upb.firerazor.units.AbstractSuperClassifier;
import ro.upb.firerazor.units.Plugin;
import ro.upb.firerazor.units.utils.ScoreVector;
import ro.upb.firerazor.utils.ACFEngineConstants;
import ro.upb.firerazor.utils.ACFEngineUtils;

import com.google.common.collect.Lists;

import de.bwaldvogel.liblinear.Feature;
import de.bwaldvogel.liblinear.FeatureNode;
import de.bwaldvogel.liblinear.Linear;
import de.bwaldvogel.liblinear.Model;
import de.bwaldvogel.liblinear.Parameter;
import de.bwaldvogel.liblinear.Problem;
import de.bwaldvogel.liblinear.SolverType;

public class LibLinSGDSuperClassfier extends AbstractSuperClassifier {
  private static final Logger logger = LoggerFactory.getLogger(LibLinSGDSuperClassfier.class);
  Model model;
  Model tempModel;

  private final DataConverter<File, ACFSample> fileConverter = new FileToACFSampleConveter();

  @Override
  public void readFromFile(File inputModel) {
    try {
      model = (Model) ACFEngineUtils.readObjectFromFile(inputModel);
    } catch (FileNotFoundException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }
  }

  private int getNumFeatures() {
    int numFeatures = 0;

    for (int i = 0; i < plugins.size(); ++i) {
      numFeatures += plugins.get(i).getOutputSize();
    }

    return numFeatures;
  }

  private ArrayList<Double> pluginsScoreToArrayList(ACFSample sample) {
    ArrayList<Double> scores = Lists.newArrayList();
    for (int i = 0; i < plugins.size(); ++i) {
      ScoreVector sv = plugins.get(i).getScore(sample);
      Iterator<Entry<String, Double>> it = sv.getEntrySet().iterator();
      while (it.hasNext()) {
        Entry<String, Double> entry = it.next();
        scores.add(entry.getValue());
        // System.out.println(plugins.get(i).getClass() + " " + entry.getKey());
      }
    }

    return scores;
  }

  @Override
  protected ScoreVector classifySample(ACFSample sample) {
    ArrayList<Double> nsample = pluginsScoreToArrayList(sample);
    Feature[] instance = new FeatureNode[nsample.size()];
    for (int i = 0; i < getNumFeatures(); ++i) {
      instance[i] = new FeatureNode(i + 1, nsample.get(i));
    }

    double score = Linear.predict(model, instance);
    ScoreVector result = new ScoreVector();
    result.setScore(ACFEngineConstants.SPAM_FOLDER, score);
    result.setScore(ACFEngineConstants.HAM_FOLDER, 1 - score);

    return result;
  }

  private void trainSubClassifiersOnFiles(final List<File> trainingFiles) {
    List<Thread> trainingThreads = Lists.newArrayList();

    for (final Plugin plugin : plugins) {
      trainingThreads.add(new Thread() {
        @Override
        public void run() {
          plugin.trainPlugin(trainingFiles);
          logger.info(String.format("Trained plugin %s", plugin));
        }
      });
    }

    for (Thread tThread : trainingThreads) {
      tThread.start();
    }

    for (Thread tThread : trainingThreads) {
      try {
        tThread.join();
      } catch (InterruptedException e) {
        logger.error("An error occured while witing for a trainig thread to finish", e);
      }
    }
  }

  protected void trainSuperModel(List<File> files) {
    int numFeatures = getNumFeatures();

    List<List<File>> trainingPartitions = Lists.partition(files, (int) (files.size() * 0.75));

    int subTrainingIndex = 0;
    int superTrainingIndex = 1;

    // train subclassifiers
    trainSubClassifiersOnFiles(trainingPartitions.get(subTrainingIndex));

    /* Get training files for super-classifier */
    Collections.shuffle(trainingPartitions.get(superTrainingIndex));
    logger.info("Starting super training step...");
    Problem problem = new Problem();
    problem.l = trainingPartitions.get(superTrainingIndex).size();
    problem.n = numFeatures;
    Feature[][] x = new Feature[trainingPartitions.get(superTrainingIndex).size()][getNumFeatures()];
    double[] y = new double[trainingPartitions.get(superTrainingIndex).size()];
    for (int f = 0; f < trainingPartitions.get(superTrainingIndex).size(); ++f) {
      ArrayList<Double> nsample = pluginsScoreToArrayList(fileConverter.convert(trainingPartitions
          .get(superTrainingIndex).get(f)));
      for (int i = 0; i < getNumFeatures(); ++i) {
        x[f][i] = new FeatureNode(i + 1, nsample.get(i));
      }
      y[f] = trainingPartitions.get(superTrainingIndex).get(f).getParentFile().getName().equals(
          ACFEngineConstants.SPAM_FOLDER) ? 1.0 : 0.0;
    }
    problem.x = x; // feature nodes
    problem.y = y; // target values

    SolverType solver = SolverType.L2R_L2LOSS_SVR_DUAL; // -s 0
    double C = 1.0; // cost of constraints violation
    double eps = 0.01; // stopping criteria

    Parameter parameter = new Parameter(solver, C, eps);
    tempModel = Linear.train(problem, parameter);
    // for (int i = 0; i < tempModel.getFeatureWeights().length; ++i) {
    // System.out.println(tempModel.getFeatureWeights()[i]);
    //
    // }

    logger.info("Ended super training step...");

    // train subclassifiers again
    trainSubClassifiersOnFiles(files);

    logger.info("Training phase done!");
  }

  @Override
  protected void getSuperModel(File trainDir) {
    trainSuperModel(ACFEngineUtils.getFilesFromPhaseDir(trainDir));
  }

  @Override
  protected void logError(String message, Throwable exception) {
    logger.error(message, exception);
  }

  @Override
  protected void writeClassifierToOutputFile(File output) {
    ACFEngineUtils.writeObjectToFile(model, output);
  }

  @Override
  protected void updateClassifier() {
    model = tempModel;
  }

  @Override
  protected void getSuperModel(File trainDir, double percent) {
    List<File> trainFiles = ACFEngineUtils.getFilesFromPhaseDir(trainDir);
    trainSuperModel(Lists.partition(trainFiles, (int) (trainFiles.size() * percent)).get(0));
  }

  @Override
  public void trainOnAll(List<File> files) {
    trainSuperModel(files);
  }

  @Override
  protected void getSuperModel(List<File> files) {
    trainSuperModel(files);
  }

}
