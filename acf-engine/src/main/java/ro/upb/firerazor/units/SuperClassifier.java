package ro.upb.firerazor.units;

import java.io.File;
import java.util.List;
import java.util.Map;

import ro.upb.firerazor.srvr.io.ACFResponse;
import ro.upb.firerazor.srvr.io.ACFSample;
import ro.upb.firerazor.units.utils.ClassifierStats;

public interface SuperClassifier extends Classifier {

  public void addPlugin(Plugin plugin);

  public List<Plugin> getPlugins();

  public void setCorpusLocation(File corpus);

  public File getCorpusLocation();

  public void train();

  public void train(double percent);

  public void trainSuper(List<File> trainFiles);

  public ClassifierStats evaluateSuper(Map<ACFSample, String> testSamples, double threshold);

  public ClassifierStats evaluate();

  public ACFResponse getClassificationResult(ACFSample sample);

  public ACFResponse getStats();

  void addPlugins(List<? extends Plugin> list);
}
