package ro.upb.firerazor.units;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.List;

import ro.upb.firerazor.srvr.io.ACFSample;
import ro.upb.firerazor.units.utils.ClassifierStats;
import ro.upb.firerazor.units.utils.ScoreVector;

/**
 * Classifier describes the behavior of a generic classifier.
 */
public interface Classifier extends Plugin {

  public void initFromFile(File inputModel) throws FileNotFoundException;

  public void initFromDefaultLocation() throws FileNotFoundException;

  public ClassifierStats evaluate(File testDir);

  public ScoreVector classify(ACFSample element);

  public void trainOnAll(List<File> files);

  public void trainBatch(File trainDir);

  public void trainBatch(File trainDir, double percent);

  public void trainOnAllBatch(List<File> trainDir);

  public void writeClassifierToFile(File outputModel);

  public void writeClassifierToDefaultLocation();

  public void setDefaultModelLocation(File output);

  public String getDefaultModelLocation();
}
