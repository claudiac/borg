package ro.upb.firerazor.units;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.Serializable;
import java.util.List;

import ro.upb.firerazor.srvr.io.ACFSample;
import ro.upb.firerazor.units.utils.ScoreVector;

/**
 * {@code Plugin} describes the behavior of a component unit used by the super
 * classifier.
 * 
 * @author Claudia Calinescu
 */
public interface Plugin extends Serializable {
  /**
   * Returns the plugin's score associated with the sample. Can only be done
   * after plugin's initialization.
   */
  public ScoreVector getScore(ACFSample sample);

  /**
   * Initializes the plugin using the data contained by the specified folder.
   */
  public void trainPlugin(File trainingFolder);

  /**
   * Initializes the plugin using the specified files.
   */
  public void trainPlugin(List<File> files);

  /**
   * Returns the size of the {@code ScoreVector} returned by the
   * {@code getScore(..)} method.
   */
  public int getOutputSize();

  public void readConfigurationFromDefaultLocation() throws FileNotFoundException;

  public void writeConfigurationToDefaultLocation();
}
