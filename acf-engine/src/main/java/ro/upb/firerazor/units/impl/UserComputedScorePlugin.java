package ro.upb.firerazor.units.impl;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ro.upb.firerazor.srvr.io.ACFSample;
import ro.upb.firerazor.units.Plugin;
import ro.upb.firerazor.units.utils.ScoreVector;
import ro.upb.firerazor.utils.ACFEngineConstants;
import ro.upb.firerazor.utils.ACFEngineUtils;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

import de.bwaldvogel.liblinear.Feature;
import de.bwaldvogel.liblinear.FeatureNode;
import de.bwaldvogel.liblinear.Linear;
import de.bwaldvogel.liblinear.Model;
import de.bwaldvogel.liblinear.Parameter;
import de.bwaldvogel.liblinear.Problem;
import de.bwaldvogel.liblinear.SolverType;

public class UserComputedScorePlugin implements Plugin, Serializable {
  private static final Logger logger = LoggerFactory.getLogger(UserComputedScorePlugin.class);
  private static final Pattern EMAIL_PATTERN = Pattern.compile("[A-Za-z0-9_.]+@[A-Za-z0-9_.]+");
  private static final File defaultConfigurationLocation = new File(ACFEngineUtils
      .getModelFileForClass(UserComputedScorePlugin.class));
  Map<String, UserNode> emailUserMap = Maps.newHashMap();
  private ArrayList<FileSample> samples = Lists.newArrayList();

  Model userNotFound;

  private static class FileSample implements Serializable {
    private UserNode source;
    private ArrayList<UserNode> destinations;
    private int isSpam;

    public FileSample(UserNode source, ArrayList<UserNode> destinations, int isSpam) {
      this.source = source;
      this.destinations = destinations;
      this.isSpam = isSpam;
    }

    public UserNode getSource() {
      return source;
    }

    public void setSource(UserNode source) {
      this.source = source;
    }

    public ArrayList<UserNode> getDestinations() {
      return destinations;
    }

    public void setDestinations(ArrayList<UserNode> destinations) {
      this.destinations = destinations;
    }

    public int getIsSpam() {
      return isSpam;
    }

    public void setIsSpam(int isSpam) {
      this.isSpam = isSpam;
    }
  }

  private static class UserNode implements Serializable {
    private final String email;
    private int spamCount;
    private int hamCount;
    private final ArrayList<UserNode> destinations;
    private final ArrayList<UserNode> sources;
    private double score;

    public double getScore() {
      return score;
    }

    public void setScore(double score) {
      this.score = score;
    }

    public UserNode(String email) {
      this.email = email;
      spamCount = 0;

      hamCount = 0;

      destinations = Lists.newArrayList();
      sources = Lists.newArrayList();
    }

    public void increaseMessageCount(String category) {
      assert (ACFEngineConstants.categories.contains(category));
      if (category.equalsIgnoreCase(ACFEngineConstants.SPAM_FOLDER)) {
        ++spamCount;
      } else {
        ++hamCount;
      }
    }

    public String getEmail() {
      return email;
    }

    public ArrayList<UserNode> getDestinations() {
      return destinations;
    }

    public ArrayList<UserNode> getSources() {
      return sources;
    }

    public double getSpamRatio() {
      if (spamCount + hamCount == 0) {
        return 0.0;
      }

      return spamCount / (double) (spamCount + hamCount);
    }

    public double getOutInRatio() {
      if (sources.size() == 0) {
        return 0.0;
      }

      return (double) destinations.size() / (double) sources.size();
    }

    public void addSource(UserNode source) {
      sources.add(source);
    }

    public void addDestination(UserNode destination) {
      destinations.add(destination);
    }
  }

  @Override
  public ScoreVector getScore(ACFSample sample) {
    String source = sample.getSrc();

    ScoreVector result = new ScoreVector();
    double score = 0.5;
    if (emailUserMap.containsKey(source)) {
      UserNode user = emailUserMap.get(source);
      score = user.getScore();
    } else {
      ArrayList<Double> destScores = Lists.newArrayList();
      for (String dest : sample.getDest().split(",")) {
        if (emailUserMap.containsKey(dest)) {
          destScores.add(emailUserMap.get(dest).getScore());
        }
      }
      if (destScores.size() > 0) {
        score = getScoreForMissingUser(destScores);
      }
    }

    result.setScore("score", score);

    return result;
  }

  public Double getUserScore(ACFSample sample) {
    String user = sample.getUser();
    double score = 0.5;
    if (emailUserMap.containsKey(user)) {
      score = emailUserMap.get(user).getScore();
    }

    return score;
  }

  private void readInputFile(File sample) {
    String source = new String();
    List<String> destinations = Lists.newArrayList();
    boolean src = false;
    boolean dest = false;

    try {
      FileReader fr = new FileReader(sample);
      BufferedReader bf = new BufferedReader(fr);

      while (bf.ready() && (!src || !dest)) {
        String line = bf.readLine();

        if (line.startsWith("To:")) {
          Matcher matcher = EMAIL_PATTERN.matcher(line);
          src = true;
          while (matcher.find()) {
            destinations.add(matcher.group());
          }
        } else {
          if (line.startsWith("From:")) {
            Matcher matcher = EMAIL_PATTERN.matcher(line);
            dest = true;
            if (matcher.find()) {
              source = matcher.group();
            }
          } else {
            continue;
          }
        }
      }

      bf.close();
      fr.close();
    } catch (IOException e) {
      logger.error(String.format("An I/O error ocurred while parsing sample file %s", sample
          .getAbsolutePath()), e);
    }

    UserNode sourceNode = null;

    if (source != null && destinations.size() > 0) {
      if (emailUserMap.containsKey(source)) {
        sourceNode = emailUserMap.get(source);
      } else {
        sourceNode = new UserNode(source);
        emailUserMap.put(source, sourceNode);
      }

      ArrayList<UserNode> sampleDestinations = Lists.newArrayList();
      sourceNode.increaseMessageCount(sample.getParentFile().getName());
      for (String destination : destinations) {
        UserNode destNode = null;
        if (emailUserMap.containsKey(destination)) {
          destNode = emailUserMap.get(destination);
        } else {
          destNode = new UserNode(destination);
          emailUserMap.put(destination, destNode);
        }
        sampleDestinations.add(destNode);
        destNode.addSource(sourceNode);
        sourceNode.addDestination(destNode);
      }

      samples.add(new FileSample(sourceNode, sampleDestinations, sample.getParentFile().getName()
          .endsWith(ACFEngineConstants.SPAM_FOLDER) ? 1 : 0));
    }
  }

  private double avg(List<Double> scores) {
    double sum = 0.0;
    for (Double score : scores) {
      sum += score;
    }
    return sum / scores.size();
  }

  private void computeUserScore() {
    Problem problem = new Problem();
    problem.l = samples.size();
    problem.n = 2;
    Feature[][] x = new Feature[samples.size()][2];
    double[] y = new double[samples.size()];
    for (int f = 0; f < samples.size(); ++f) {
      x[f][0] = new FeatureNode(1, samples.get(f).getSource().getSpamRatio());
      x[f][1] = new FeatureNode(2, samples.get(f).getSource().getOutInRatio());
      y[f] = samples.get(f).getIsSpam();
    }
    problem.x = x;
    problem.y = y;

    SolverType solver = SolverType.L2R_L2LOSS_SVR_DUAL;
    double C = 1.0;
    double eps = 0.01;

    Parameter parameter = new Parameter(solver, C, eps);
    Model model = Linear.train(problem, parameter);

    for (Map.Entry<String, UserNode> entry : emailUserMap.entrySet()) {
      Feature[] instance = new FeatureNode[2];
      instance[0] = new FeatureNode(1, entry.getValue().getSpamRatio());
      instance[1] = new FeatureNode(2, entry.getValue().getOutInRatio());
      double score = Linear.predict(model, instance);
      entry.getValue().setScore(score);
    }
  }

  private void perfectUserScoreIteration() {
    Problem problem = new Problem();
    problem.l = samples.size();
    problem.n = 4;
    Feature[][] x = new Feature[samples.size()][4];
    double[] y = new double[samples.size()];
    for (int f = 0; f < samples.size(); ++f) {
      List<Double> destScores = Lists.newArrayList();
      for (UserNode dest : samples.get(f).getSource().getDestinations()) {
        destScores.add(dest.getScore());
      }

      for (UserNode dest : samples.get(f).getSource().getSources()) {
        destScores.add(dest.getScore());
      }

      x[f][0] = new FeatureNode(1, samples.get(f).getSource().getScore());
      x[f][1] = new FeatureNode(2, Collections.max(destScores));
      x[f][2] = new FeatureNode(3, Collections.min(destScores));
      x[f][3] = new FeatureNode(4, avg(destScores));

      y[f] = samples.get(f).getIsSpam();
    }
    problem.x = x;
    problem.y = y;

    SolverType solver = SolverType.L2R_L2LOSS_SVR_DUAL;
    double C = 1.0;
    double eps = 0.01;

    Parameter parameter = new Parameter(solver, C, eps);
    Model model = Linear.train(problem, parameter);

    Map<UserNode, Double> newScore = Maps.newHashMap();
    for (Map.Entry<String, UserNode> entry : emailUserMap.entrySet()) {
      Feature[] instance = new FeatureNode[4];
      List<Double> destScores = Lists.newArrayList();
      for (UserNode dest : entry.getValue().getDestinations()) {
        destScores.add(dest.getScore());
      }

      for (UserNode dest : entry.getValue().getSources()) {
        destScores.add(dest.getScore());
      }

      instance[0] = new FeatureNode(1, entry.getValue().getScore());
      instance[1] = new FeatureNode(2, Collections.max(destScores));
      instance[2] = new FeatureNode(3, Collections.min(destScores));
      instance[3] = new FeatureNode(4, avg(destScores));

      double score = Linear.predict(model, instance);

      newScore.put(entry.getValue(), score);
    }

    for (Map.Entry<UserNode, Double> entry : newScore.entrySet()) {
      entry.getKey().setScore(entry.getValue());
    }
  }

  private double getScoreForMissingUser(List<Double> destScores) {
    Feature[] instance = new FeatureNode[3];
    for (int i = 0; i < 3; ++i) {
      instance[i] = new FeatureNode(i + 1, 0);
    }

    instance[0] = new FeatureNode(1, Collections.max(destScores));
    instance[1] = new FeatureNode(2, Collections.min(destScores));
    instance[2] = new FeatureNode(3, avg(destScores));

    return Linear.predict(userNotFound, instance);
  }

  private void computeModelForMissingUser() {
    Problem problem1 = new Problem();
    problem1.l = samples.size();
    problem1.n = 3;
    Feature[][] x1 = new Feature[samples.size()][3];
    double[] y1 = new double[samples.size()];
    for (int f = 0; f < samples.size(); ++f) {
      for (int i = 0; i < 3; ++i) {
        x1[f][i] = new FeatureNode(i + 1, 0);
      }
      ArrayList<Double> destScores = Lists.newArrayList();
      for (UserNode dest : samples.get(f).getDestinations()) {
        destScores.add(dest.getScore());
      }
      if (destScores.size() > 0) {
        x1[f][0] = new FeatureNode(1, Collections.max(destScores));
        x1[f][1] = new FeatureNode(2, Collections.min(destScores));
        x1[f][2] = new FeatureNode(3, avg(destScores));
      }
      y1[f] = samples.get(f).getIsSpam();
    }

    problem1.x = x1; // feature nodes
    problem1.y = y1; // target values

    SolverType solver1 = SolverType.L2R_L2LOSS_SVR_DUAL; // -s 0
    double C1 = 1.0; // cost of constraints violation
    double eps1 = 0.01; // stopping criteria

    Parameter parameter1 = new Parameter(solver1, C1, eps1);
    userNotFound = Linear.train(problem1, parameter1);
  }

  @Override
  public void trainPlugin(File trainingFolder) {
    // Compute spam/ham score and out/in ratio
    List<File> files = Lists.newArrayList();
    for (File category : trainingFolder.listFiles()) {
      if (!ACFEngineConstants.categories.contains(category.getName())) {
        continue;
      }
      for (File sample : category.listFiles()) {
        files.add(sample);
      }
    }

    trainPlugin(files);
  }

  @Override
  public void trainPlugin(List<File> files) {
    emailUserMap.clear();
    for (File sample : files) {
      readInputFile(sample);
    }

    computeUserScore();
    for (int i = 0; i < 10; ++i) {
      perfectUserScoreIteration();
    }
    computeModelForMissingUser();
  }

  @Override
  public int getOutputSize() {
    return 1;
  }

  @Override
  public void readConfigurationFromDefaultLocation() throws FileNotFoundException {
    try {
      UserComputedScorePlugin nPlugin = (UserComputedScorePlugin) ACFEngineUtils
          .readObjectFromFile(defaultConfigurationLocation);
      this.emailUserMap = nPlugin.emailUserMap;
      this.userNotFound = nPlugin.userNotFound;
      this.samples = nPlugin.samples;
    } catch (FileNotFoundException e) {
      throw new FileNotFoundException();
    }
  }

  @Override
  public void writeConfigurationToDefaultLocation() {
    ACFEngineUtils.writeObjectToFile(this, defaultConfigurationLocation);
  }
}
