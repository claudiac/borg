package ro.upb.firerazor.units.utils;

import ro.upb.firerazor.utils.ACFEngineConstants;

public class ClassifierAnalyzer {
  static final int REAL_HAM = 0;
  static final int REAL_SPAM = 1;

  static final int PRED_HAM = 0;
  static final int PRED_SPAM = 1;

  private final Integer[][] conf_matrix;

  private double relativeError = 0;

  public ClassifierAnalyzer() {
    conf_matrix = new Integer[2][2];
    for (int i = 0; i < 2; ++i) {
      for (int j = 0; j < 2; ++j) {
        conf_matrix[i][j] = new Integer(0);
      }
    }
  }

  public void recordAnswer(String actualCat, String deducedCat, double value) {
    if (actualCat.equals(ACFEngineConstants.SPAM_FOLDER)) {
      if (actualCat.equals(deducedCat)) {
        ++conf_matrix[PRED_SPAM][REAL_SPAM];
      } else {
        ++conf_matrix[PRED_HAM][REAL_SPAM];
      }
    } else {
      if (actualCat.equals(deducedCat)) {
        ++conf_matrix[PRED_HAM][REAL_HAM];
      } else {
        ++conf_matrix[PRED_SPAM][REAL_HAM];
      }
    }

    relativeError += Math.abs(1 - value);
  }

  public Double getRelativeError() {
    double total = conf_matrix[PRED_HAM][REAL_HAM] + conf_matrix[PRED_HAM][REAL_SPAM]
        + conf_matrix[PRED_SPAM][REAL_HAM] + conf_matrix[PRED_SPAM][REAL_SPAM];
    return new Double(relativeError / total);
  }

  public Double getPrecision() {
    return new Double(conf_matrix[PRED_HAM][REAL_HAM]
        / (double) (conf_matrix[PRED_HAM][REAL_HAM] + conf_matrix[PRED_HAM][REAL_SPAM]));
  }

  public Double getAccuracy() {
    double total = conf_matrix[PRED_HAM][REAL_HAM] + conf_matrix[PRED_HAM][REAL_SPAM]
        + conf_matrix[PRED_SPAM][REAL_HAM] + conf_matrix[PRED_SPAM][REAL_SPAM];
    double correct = conf_matrix[PRED_HAM][REAL_HAM] + conf_matrix[PRED_SPAM][REAL_SPAM];

    return new Double(correct / total);
  }

  public Double getSensitivity() {
    return new Double(conf_matrix[PRED_HAM][REAL_HAM]
        / (double) (conf_matrix[PRED_HAM][REAL_HAM] + conf_matrix[PRED_SPAM][REAL_HAM]));
  }

  public Double getSpecificity() {
    return new Double(conf_matrix[PRED_SPAM][REAL_SPAM]
        / (double) (conf_matrix[PRED_SPAM][REAL_SPAM] + conf_matrix[PRED_HAM][REAL_SPAM]));
  }

  public Double getRecall() {
    return new Double(conf_matrix[PRED_HAM][REAL_HAM]
        / (double) (conf_matrix[PRED_HAM][REAL_HAM] + conf_matrix[PRED_SPAM][REAL_HAM]));
  }

  public ClassifierStats getStats() {
    ClassifierStats result = new ClassifierStats();
    result.setAccuracy(getAccuracy());
    result.setConfusionMatrix(conf_matrix);
    result.setSensitivity(getSensitivity());
    result.setSpecificity(getSpecificity());
    result.setPrecision(getPrecision());
    result.setRecall(getRecall());
    result.setRelativeError(getRelativeError());

    return result;
  }
}
