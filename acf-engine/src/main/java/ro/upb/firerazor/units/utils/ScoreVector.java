package ro.upb.firerazor.units.utils;

import java.util.Iterator;
import java.util.Map.Entry;
import java.util.Set;
import java.util.SortedMap;

import com.google.common.collect.Maps;

public class ScoreVector {
  private final SortedMap<String, Double> content = Maps.newTreeMap();

  public void setScore(String label, Double score) {
    content.put(label, score);
  }

  public Double getScore(String label) {
    return content.get(label);
  }

  public Set<Entry<String, Double>> getEntrySet() {
    return content.entrySet();
  }

  public Set<String> getOrderedLabels() {
    return content.keySet();
  }

  public String getBestLabel() {
    if (content.size() == 0) {
      return null;
    }

    double max = Double.MIN_VALUE;
    String best = "";

    Iterator<Entry<String, Double>> it = content.entrySet().iterator();

    while (it.hasNext()) {
      Entry<String, Double> crt = it.next();
      if (crt.getValue() > max) {
        max = crt.getValue();
        best = crt.getKey();
      }
    }

    return best;
  }

  @Override
  public String toString() {
    StringBuffer sbuf = new StringBuffer();
    Iterator<Entry<String, Double>> it = content.entrySet().iterator();

    while (it.hasNext()) {
      Entry<String, Double> crt = it.next();
      sbuf.append(String.format(" ('%s': %f) ", crt.getKey(), crt.getValue()));
    }

    return sbuf.toString();
  }
}
