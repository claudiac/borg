package ro.upb.firerazor.units;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.List;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

import ro.upb.firerazor.data.impl.FileToACFSampleConveter;
import ro.upb.firerazor.srvr.io.ACFSample;
import ro.upb.firerazor.units.utils.ClassifierAnalyzer;
import ro.upb.firerazor.units.utils.ClassifierStats;
import ro.upb.firerazor.units.utils.ScoreVector;
import ro.upb.firerazor.utils.ACFEngineConstants;
import ro.upb.firerazor.utils.ACFEngineUtils;

import com.google.common.base.Preconditions;
import com.google.common.base.Predicate;
import com.google.common.collect.FluentIterable;

public abstract class AbstractClassifier implements Classifier {
  protected File defaultModelLocation;
  protected boolean isTrained = false;

  protected final ReadWriteLock confLock = new ReentrantReadWriteLock();
  protected final Lock readConfLock = confLock.readLock();
  protected final Lock writeConfLock = confLock.writeLock();

  public AbstractClassifier() {
    defaultModelLocation = createDefaultModelFile();
  }

  @Override
  public void trainPlugin(File trainDir) {
    trainBatch(trainDir);
  }

  @Override
  public void trainPlugin(List<File> files) {
    trainOnAll(files);
    updateClassifierModel();
  }

  @Override
  public void readConfigurationFromDefaultLocation() throws FileNotFoundException {
    initFromDefaultLocation();
  }

  @Override
  public void writeConfigurationToDefaultLocation() {
    writeClassifierToDefaultLocation();
  }

  @Override
  public synchronized void initFromFile(File inputModel) throws FileNotFoundException {
    if (!(inputModel.exists())) {
      throw new FileNotFoundException();
    }

    writeConfLock.lock();
    readFromFile(inputModel);
    writeConfLock.unlock();

    isTrained = true;
  }

  @Override
  public ClassifierStats evaluate(File testDir) {
    Preconditions.checkArgument(testDir.exists() && testDir.isDirectory());

    if (!isTrained) {
      logError("Classifier was not initialized!", new UninitializedClassifierException());
      return null;
    }

    ClassifierStats result;

    readConfLock.lock();
    result = evaluateClassifier(testDir);
    readConfLock.unlock();

    return result;
  }

  protected ClassifierStats evaluateClassifier(File testDir) {
    ClassifierAnalyzer analyzer = new ClassifierAnalyzer();

    FileToACFSampleConveter converter = new FileToACFSampleConveter();

    for (File category : testDir.listFiles()) {
      if (!ACFEngineConstants.categories.contains(category.getName())) {
        continue;
      }
      for (File sample : category.listFiles()) {
        ScoreVector score = classify(converter.convert(sample));
        analyzer.recordAnswer(category.getName(), score.getBestLabel(), score.getScore(score
            .getBestLabel()));
      }
    }

    return analyzer.getStats();
  }

  @Override
  public ScoreVector classify(ACFSample element) {
    if (!isTrained) {
      logError(String.format("Classifier was not initialized! [%s]", this.getClass().toString()),
          new UninitializedClassifierException());
      return null;
    }

    ScoreVector result;

    readConfLock.lock();
    result = classifySample(element);
    readConfLock.unlock();

    return result;
  }

  @Override
  public void trainBatch(File trainDir) {
    System.out.println(trainDir.getName() + " -- " + trainDir.getAbsolutePath());
    Preconditions.checkArgument(trainDir.exists() && trainDir.isDirectory());

    trainModel(trainDir);
    updateClassifierModel();
  }

  @Override
  public void trainBatch(File trainDir, double percent) {
    System.out.println(trainDir.getName() + " -- " + trainDir.getAbsolutePath());
    Preconditions.checkArgument(trainDir.exists() && trainDir.isDirectory());

    trainModel(trainDir, percent);
    updateClassifierModel();
    // throw new UnsupportedOperationException();
  }

  @Override
  public void trainOnAllBatch(List<File> trainDirs) {
    Preconditions.checkArgument(FluentIterable.from(trainDirs).allMatch(new Predicate<File>() {
      @Override
      public boolean apply(File input) {
        return input.exists() && input.isDirectory();
      }
    }));

    trainModel(trainDirs);
    updateClassifierModel();
  }

  private synchronized void updateClassifierModel() {
    isTrained = true;

    writeConfLock.lock();
    updateClassifier();
    writeConfLock.unlock();
  }

  @Override
  public synchronized void writeClassifierToFile(File outputModel) {
    if (!isTrained) {
      logError("Classifier was not initialized!", new UninitializedClassifierException());
    }

    readConfLock.lock();
    writeClassifierToOutputFile(outputModel);
    readConfLock.unlock();
  }

  @Override
  public synchronized void setDefaultModelLocation(File output) {
    defaultModelLocation = output;
  }

  @Override
  public void writeClassifierToDefaultLocation() {
    writeClassifierToFile(defaultModelLocation);
  }

  @Override
  public String getDefaultModelLocation() {
    return defaultModelLocation.getAbsolutePath();
  }

  @Override
  public ScoreVector getScore(ACFSample sample) {
    return classify(sample);
  }

  @Override
  public int getOutputSize() {
    return ACFEngineConstants.categories.size();
  }

  @Override
  public void initFromDefaultLocation() throws FileNotFoundException {
    initFromFile(defaultModelLocation);
  }

  protected File createDefaultModelFile() {
    return new File(ACFEngineConstants.DEFAULT_MODEL_FOLDER, ACFEngineUtils
        .getModelFileForClass(this.getClass()));
  }

  protected abstract void updateClassifier();

  protected abstract void readFromFile(File inputModel);

  protected abstract ScoreVector classifySample(ACFSample sample);

  protected abstract void trainModel(File trainDir);

  protected void trainModel(File trainDir, double percent) {
    throw new UnsupportedOperationException();
  }

  protected abstract void trainModel(List<File> trainDir);

  protected abstract void logError(String message, Throwable exception);

  protected abstract void writeClassifierToOutputFile(File output);

  @SuppressWarnings("serial")
  public static class UninitializedClassifierException extends Exception {
    public UninitializedClassifierException() {
      super();
    }
  }

}
