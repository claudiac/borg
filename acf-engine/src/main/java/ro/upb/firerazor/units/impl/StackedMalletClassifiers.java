package ro.upb.firerazor.units.impl;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.Map.Entry;
import java.util.SortedMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ro.upb.firerazor.data.impl.FolderToMalletInstanceListConvertor;
import ro.upb.firerazor.liblintomltsvm.SVMClassifier;
import ro.upb.firerazor.liblintomltsvm.SVMClassifierTrainer;
import ro.upb.firerazor.srvr.io.ACFSample;
import ro.upb.firerazor.units.AbstractClassifier;
import ro.upb.firerazor.units.utils.ScoreVector;
import ro.upb.firerazor.utils.ACFEngineConstants;
import ro.upb.firerazor.utils.ACFEngineUtils;
import ca.uwo.csd.ai.nlp.kernel.LinearKernel;
import cc.mallet.classify.Classification;
import cc.mallet.classify.Classifier;
import cc.mallet.classify.ClassifierTrainer;
import cc.mallet.types.InstanceList;
import cc.mallet.types.Labeling;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

public class StackedMalletClassifiers extends AbstractClassifier {
  private final static Logger logger = LoggerFactory.getLogger(StackedMalletClassifiers.class);
  private final SortedMap<Class, InternalClassifier> internalClassifiers = Maps
      .newTreeMap(new Comparator<Class>() {
        @Override
        public int compare(Class o1, Class o2) {
          return o1.toString().compareTo(o2.toString());
        }
      });

  private static class InternalClassifier {
    private Classifier classifier;
    private ClassifierTrainer trainer;

    public Classifier getClassifier() {
      return classifier;
    }

    public void setClassifier(Classifier classifier) {
      this.classifier = classifier;
    }

    public ClassifierTrainer getTrainer() {
      return trainer;
    }

    public void setTrainer(ClassifierTrainer trainer) {
      this.trainer = trainer;
    }

    public void updateClassifier() {
      classifier = trainer.getClassifier();
    }
  }

  public StackedMalletClassifiers(Object list) {
    super();
    for (Class trainerClass : (List<Class>) list) {
      internalClassifiers.put(trainerClass, new InternalClassifier());
    }

    defaultModelLocation = new File(ACFEngineConstants.DEFAULT_MODEL_FOLDER, ACFEngineUtils
        .getModelFileForClass(this.getClass()));
    defaultModelLocation.mkdir();
  }

  @Override
  public void readFromFile(File inputModel) {
    Iterator<Entry<Class, InternalClassifier>> it = internalClassifiers.entrySet().iterator();
    while (it.hasNext()) {
      Entry<Class, InternalClassifier> entry = it.next();
      File input = new File(inputModel, ACFEngineUtils.getModelFileForClass(entry.getKey()));
      try {
        if (entry.getKey().equals(SVMClassifierTrainer.class)) {
          entry.getValue().setClassifier(
              (SVMClassifier) new ObjectInputStream(new FileInputStream(input)).readObject());
        } else {
          entry.getValue().setClassifier(
              (Classifier) new ObjectInputStream(new FileInputStream(input)).readObject());
        }
      } catch (FileNotFoundException e) {
        logger.error(String.format("The specified model file [%s] does not exist!", input
            .getAbsolutePath()), e);
      } catch (IOException e) {
        logger.error(String.format("An error occured while reading the mode file [%s].", input
            .getAbsoluteFile()), e);
      } catch (ClassNotFoundException e) {
        logger.error(String.format("An error occured while reading the mode file [%s].", input
            .getAbsoluteFile()), e);
      }
    }
  }

  @Override
  public ScoreVector classifySample(ACFSample element) {
    ScoreVector result = new ScoreVector();

    Iterator<Entry<Class, InternalClassifier>> it = internalClassifiers.entrySet().iterator();
    while (it.hasNext()) {
      Classifier classifier = it.next().getValue().getClassifier();
      Classification c = classifier.classify(element.getBody());

      Labeling labeling = c.getLabeling();

      result.setScore(classifier.getClass()
          + classifier.getLabelAlphabet().lookupLabel(0).toString(), labeling.value(classifier
          .getLabelAlphabet().lookupLabel(0)));
      result.setScore(classifier.getClass()
          + classifier.getLabelAlphabet().lookupLabel(1).toString(), labeling.value(classifier
          .getLabelAlphabet().lookupLabel(1)));

    }
    return result;
  }

  @Override
  protected void logError(String message, Throwable exception) {
    logger.error(message, exception);
  }

  @Override
  protected void writeClassifierToOutputFile(File output) {
    Iterator<Entry<Class, InternalClassifier>> it = internalClassifiers.entrySet().iterator();
    while (it.hasNext()) {
      Entry<Class, InternalClassifier> entry = it.next();
      Classifier classifier = entry.getValue().getClassifier();
      ObjectOutputStream oos;
      File outputFile = new File(output, ACFEngineUtils.getModelFileForClass(entry.getKey()));
      try {
        oos = new ObjectOutputStream(new FileOutputStream(outputFile));
        oos.writeObject(classifier);
        oos.close();
      } catch (FileNotFoundException e) {
        logger.error(String.format("The specified output model file [%s] does not exist",
            outputFile.getAbsolutePath()), e);
      } catch (IOException e) {
        logger.error(String.format("An error occured while writing the model to file [%s]",
            outputFile.getAbsolutePath()), e);
      }
    }
  }

  @Override
  protected void updateClassifier() {
    Iterator<Entry<Class, InternalClassifier>> it = internalClassifiers.entrySet().iterator();
    while (it.hasNext()) {
      it.next().getValue().updateClassifier();
    }
  }

  @Override
  protected void trainModel(File trainDir) {
    trainModel(Arrays.asList(trainDir));
  }

  @Override
  protected void trainModel(List<File> trainDir) {
    trainOnInstances(new FolderToMalletInstanceListConvertor().convert(trainDir));
  }

  private void trainOnInstances(final InstanceList instances) {
    Iterator<Entry<Class, InternalClassifier>> it = internalClassifiers.entrySet().iterator();
    List<Thread> trainingThreads = Lists.newArrayList();

    while (it.hasNext()) {
      final Entry<Class, InternalClassifier> entry = it.next();
      if (entry.getKey().equals(SVMClassifierTrainer.class)) {
        entry.getValue().setTrainer(new SVMClassifierTrainer(new LinearKernel()));
      } else {
        try {
          entry.getValue().setTrainer((ClassifierTrainer) entry.getKey().newInstance());
        } catch (Exception e) {
          logger.error(String.format(
              "An error occured while trying to get an instance for trainer [%s]", entry.getKey()),
              e);
        }
      }

      trainingThreads.add(new Thread() {
        @Override
        public void run() {
          entry.getValue().getTrainer().train(instances);
          logger.info(String.format("Trained [%s]", entry.getValue().getTrainer().getClass()));
        }
      });

    }

    for (Thread thread : trainingThreads) {
      thread.start();
    }

    for (Thread thread : trainingThreads) {
      try {
        thread.join();
      } catch (InterruptedException e) {
        logger.error("Trainig thread was interrupted!", e);
      }
    }
  }

  @Override
  public void trainOnAll(List<File> files) {
    trainOnInstances(new FolderToMalletInstanceListConvertor().convertFiles(files));
    updateClassifier();
    isTrained = true;
  }

  @Override
  public int getOutputSize() {
    return internalClassifiers.size() * 2;
  }
}
