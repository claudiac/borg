package ro.upb.firerazor.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.Arrays;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ro.upb.firerazor.data.impl.FileToACFSampleConveter;
import ro.upb.firerazor.srvr.io.ACFSample;

import com.google.common.base.Function;
import com.google.common.base.Predicate;
import com.google.common.collect.FluentIterable;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

public class ACFEngineUtils {
  private static final Logger logger = LoggerFactory.getLogger(ACFEngineUtils.class);

  public static void splitData(final File baseDir) throws InvalidFolderStructureException {
    if (!(baseDir.exists() && baseDir.isDirectory())) {
      throw new IllegalArgumentException("The specified file is not a directory");
    }
    List<File> contentFiles = Arrays.asList(baseDir.listFiles());

    /**
     * Check whether the folder has the desired structure. First check for
     * phases folders.
     */
    List<File> phaseFolders = FluentIterable.from(contentFiles).filter(new Predicate<File>() {
      @Override
      public boolean apply(File input) {
        return input.isDirectory() && ACFEngineConstants.phases.contains(input.getName());
      }
    }).toImmutableList();

    if (phaseFolders.size() == ACFEngineConstants.phases.size()) {
      /**
       * Check for categories folders in phases folders
       */
      boolean correctStructure = FluentIterable.from(phaseFolders).allMatch(new Predicate<File>() {
        @Override
        public boolean apply(File input) {
          return FluentIterable.from(Arrays.asList(input.listFiles())).filter(
              new Predicate<File>() {
                @Override
                public boolean apply(File input) {
                  return input.isDirectory()
                      && ACFEngineConstants.categories.contains(input.getName());
                }
              }).toImmutableList().size() == ACFEngineConstants.categories.size();
        }
      });

      if (correctStructure) {
        logger.info("Corpus structure was valid. There was no need for splitting.");
        return;
      } else {
        throw new InvalidFolderStructureException(
            "The phases folders in the corpus did not have categories folders in them.");
      }
    }

    /**
     * Check for category folders.
     */
    List<File> categoryFolders = FluentIterable.from(contentFiles).filter(new Predicate<File>() {
      @Override
      public boolean apply(File input) {
        return ACFEngineConstants.categories.contains(input.getName()) && input.isDirectory();
      }
    }).toImmutableList();

    if (categoryFolders.size() != ACFEngineConstants.categories.size()) {
      throw new InvalidFolderStructureException(
          "Corpus folder does not contain phases or category folders at the first level.");
    }

    /**
     * Do the actual splitting. Create phase folders.
     */
    List<File> phases = FluentIterable.from(ACFEngineConstants.phases).transform(
        new Function<String, File>() {
          @Override
          public File apply(String input) {
            return createDir(baseDir, input);
          }
        }).toImmutableList();
    logger.info("Created phase folders.");

    for (File category : categoryFolders) {
      /**
       * Create category folders in phase folders.
       */
      Map<String, File> phaseCat = Maps.newHashMap();
      for (File phase : phases) {
        phaseCat.put(phase.getName(), createDir(phase, category.getName()));
      }
      logger.info(String.format("Created %s folders for phases.", category.getName()));

      /**
       * Assign random samples between phases
       */
      for (File entity : category.listFiles()) {
        double randValue = Math.random();

        Iterator<Entry<Double, String>> it = ACFEngineConstants.percentageLevel.entrySet()
            .iterator();
        while (it.hasNext()) {
          Entry<Double, String> crtEntry = it.next();
          if (randValue < crtEntry.getKey().doubleValue()) {
            entity.renameTo(new File(phaseCat.get(crtEntry.getValue()), entity.getName()));
          }
        }
      }

      category.delete();
      logger.info(String.format("Distributed %s files between phases.", category.getName()));
    }

    logger.info("Done!");
  }

  private static File createDir(File parent, String name) {
    File dir = new File(parent, name);
    dir.mkdir();

    return dir;
  }

  public static class InvalidFolderStructureException extends Exception {
    public InvalidFolderStructureException(String message) {
      super(message);
    }

    public InvalidFolderStructureException() {
      super();
    }
  }

  public static String getModelFileForClass(Class input) {
    return String.format("classfier_%s.model", input.toString().substring("class ".length()));
  }

  public static String getEvaluationFileForClass(Class input) {
    return String.format("classfier_%s.eval", input.toString().substring("class ".length()));
  }

  public static List<File> getFilesFromPhaseDir(File phaseDir) {
    if (!phaseDir.exists() || !phaseDir.isDirectory()) {
      throw new IllegalArgumentException();
    }

    List<File> files = Lists.newArrayList();
    for (File category : phaseDir.listFiles()) {
      if (!ACFEngineConstants.categories.contains(category.getName())) {
        continue;
      }
      for (File sample : category.listFiles()) {
        if (sample.isFile()) {
          files.add(sample);
        }
      }
    }

    Collections.shuffle(files);

    return files;
  }

  public static List<File> getFilesFromCorpus(File corpus) {
    if (!corpus.exists() || !corpus.isDirectory()) {
      throw new IllegalArgumentException();
    }

    List<File> files = Lists.newArrayList();
    for (File phase : corpus.listFiles()) {
      if (!phase.isDirectory() || !ACFEngineConstants.phases.contains(phase.getName())) {
        continue;
      }
      for (File category : phase.listFiles()) {
        if (!phase.isDirectory() || !ACFEngineConstants.categories.contains(category.getName())) {
          continue;
        }
        for (File sample : category.listFiles()) {
          if (sample.isFile()) {
            files.add(sample);
          }
        }
      }
    }

    Collections.shuffle(files);

    return files;

  }

  public static Map<ACFSample, String> getACFSamplesFromFiles(List<File> files) {
    Map<ACFSample, String> map = Maps.newHashMap();
    FileToACFSampleConveter converter = new FileToACFSampleConveter();

    for (File file : files) {
      map.put(converter.convert(file), file.getParentFile().getName());
    }

    return map;
  }

  public static void writeObjectToFile(Serializable obj, File file) {
    ObjectOutputStream oos;
    try {
      oos = new ObjectOutputStream(new FileOutputStream(file));
      oos.writeObject(obj);
      oos.close();
    } catch (FileNotFoundException e) {
      logger.error(String.format("The specified output file [%s] does not exist", file
          .getAbsolutePath()), e);
    } catch (IOException e) {
      logger.error(String.format("An error occured while writing the object to file [%s]", file
          .getAbsolutePath()), e);
    }
  }

  public static Serializable readObjectFromFile(File file) throws FileNotFoundException {
    Serializable obj;

    try {
      obj = (Serializable) new ObjectInputStream(new FileInputStream(file)).readObject();
      return obj;
    } catch (FileNotFoundException e) {
      logger.error(
          String.format("The specified file [%s] does not exist!", file.getAbsoluteFile()), e);
      throw new FileNotFoundException();
    } catch (IOException e) {
      logger.error(String.format("An error occured while reading the object from file [%s].", file
          .getAbsoluteFile()), e);
    } catch (ClassNotFoundException e) {
      logger.error(String.format("An error occured while reading the object from file [%s].", file
          .getAbsoluteFile()), e);
    }

    return null;
  }
}
