package ro.upb.firerazor.utils;

import java.util.Arrays;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.SortedMap;
import java.util.TreeMap;

import com.google.common.collect.Maps;

public class ACFEngineConstants {
  public static final String SPAM_FOLDER = "spam";
  public static final String HAM_FOLDER = "ham";
  public static final String TRAINING_FOLDER = "training";
  public static final String TESTING_FOLDER = "testing";
  public static final String AGGREGATION_FOLDER = "aggregation";

  public static final String DEFAULT_MODEL_FOLDER = "/Users/calinesc/Documents/workspace/acf/acf-engine";

  public static final int folds = 10;

  public static final List<String> categories = Arrays.asList(new String[] { HAM_FOLDER,
      SPAM_FOLDER });

  public static final List<String> phases = Arrays.asList(new String[] { TRAINING_FOLDER,
      TESTING_FOLDER });

  private static final Map<String, Double> dataPerPhase = createPercentageMap();

  private static Map<String, Double> createPercentageMap() {
    Map<String, Double> result = Maps.newHashMap();
    result.put(TRAINING_FOLDER, 0.8);
    result.put(TESTING_FOLDER, 0.2);
    // result.put(AGGREGATION_FOLDER, 0.2);
    return Collections.unmodifiableMap(result);
  }

  public static final SortedMap<Double, String> percentageLevel = createPercentageLevelMap();

  private static SortedMap<Double, String> createPercentageLevelMap() {
    SortedMap<Double, String> result = new TreeMap<Double, String>();

    double level = 0;

    Iterator<Entry<String, Double>> it = dataPerPhase.entrySet().iterator();

    while (it.hasNext()) {
      Entry<String, Double> crtEntry = it.next();
      level += crtEntry.getValue().doubleValue();
      result.put(level, crtEntry.getKey());
    }

    return result;
  }
}
