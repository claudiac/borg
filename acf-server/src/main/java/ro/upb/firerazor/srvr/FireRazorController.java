package ro.upb.firerazor.srvr;

import java.io.FileNotFoundException;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import ro.upb.firerazor.main.ACFEngine;
import ro.upb.firerazor.srvr.io.ACFIoConstants;
import ro.upb.firerazor.srvr.io.ACFSample;
import ro.upb.firerazor.srvr.io.ACFResponse;
import ro.upb.firerazor.srvr.utils.ACFServerConstants;

@Controller
@RequestMapping(value = "/**")
public class FireRazorController {
	private final static Logger logger = LoggerFactory.getLogger(FireRazorController.class);
	public static final String DEFAULT_CORPUS_LOCATION = "/Users/calinesc/Documents/workspace/acf/acf-engine/sacorpus";
	
	private ACFEngine engine;
	private Thread masterThread;
	private boolean doStuff;
	
	@PostConstruct
	public void initialize() {
		logger.info("Initializing servlet ...");
		
	    engine = new ACFEngine(DEFAULT_CORPUS_LOCATION);
	    logger.info("Initialized engine ...");
	    
	    doStuff = true;
	    masterThread = new Thread(){
	    	@Override
	    	public void run() {
//	    		try {
//	    			engine.loadDefaultConfiguration();
//	    			logger.info("Found default configuration!\nLoading from default configuration...");
//	    		} catch (FileNotFoundException e) {
//	    			logger.info("No configuration file found!");
//				}
	    		
	    		while (doStuff) {
	    			double startTime = System.currentTimeMillis();
	    			logger.info("Starting training round ...");
	    			engine.train();
	    			logger.info(String.format("Done training in %f minutes!",
	    					(System.currentTimeMillis() - startTime)/1000/60));
	    			
	    			logger.info("Writing model to default location...");
	    			engine.writeDefaultConfiguration();
	    			logger.info("Done writing model!");
	    		}
	    	}
	    };
	    
	    logger.info("Starting master thread ...");
	    masterThread.start();
	    logger.info("Done initializing servlet!");
	}

	@RequestMapping(value = ACFServerConstants.ACTION_CLASSIFY, method = RequestMethod.PUT)
	public @ResponseBody ResponseEntity<ACFResponse> classify(@RequestBody ACFSample req) {
		logger.info("Received classify reguest!");
		double startTime = System.currentTimeMillis();
		
		ACFResponse result = new ACFResponse();
		result.setResponseType(ACFIoConstants.ENGINE_NOT_INITIALIZED);
		if (engine.isInitialized()) {
			result = engine.classify(req);
		}
		
		logger.info(String.format("Classified in %f miliseconds [spamicity: %f]",
				System.currentTimeMillis() - startTime, result.getSpamicity()));
		
		return new ResponseEntity<ACFResponse>(result, HttpStatus.OK);
	}
	
	@RequestMapping(value = ACFServerConstants.ACTION_USER_REPUTATION, method = RequestMethod.PUT)
	public @ResponseBody ResponseEntity<ACFResponse> userReputatin(@RequestBody ACFSample req) {
		logger.info("Received user reputation reguest!");
		double startTime = System.currentTimeMillis();
		
		ACFResponse result = new ACFResponse();
		result.setResponseType(ACFIoConstants.ENGINE_NOT_INITIALIZED);
		if (engine.isInitialized()) {
			result = engine.getUserReputation(req);
		}
		
		logger.info(String.format("Computed score in %f miliseconds [user reputation: %f]",
				System.currentTimeMillis() - startTime, result.getUserReputation()));
		
		return new ResponseEntity<ACFResponse>(result, HttpStatus.OK);
	}
	
	@RequestMapping(value = ACFServerConstants.ACTION_STATS, method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<ACFResponse> stats() {
		logger.info("Received classifier evaluation request!");
		double startTime = System.currentTimeMillis();
		
		ACFResponse result = new ACFResponse();
		result.setResponseType(ACFIoConstants.ENGINE_NOT_INITIALIZED);
		if (engine.isInitialized()) {
			result = engine.getStats();
		}
		
		logger.info(String.format("Evaluation obtained in %f miliseconds", 
				System.currentTimeMillis() - startTime));
		
		return new ResponseEntity<ACFResponse>(result, HttpStatus.OK);
	}	
	
	@PreDestroy
	public void destroy() {
		logger.info("Trying to kill the master trainer thread...");
		doStuff = false;
		masterThread.interrupt();
		logger.info("Master thread killed!");
	}
}