package ro.upb.firerazor.srvr.utils;

public class ACFServerConstants {
	public static final String ACTION_CLASSIFY = "classify";
	public static final String ACTION_STATS = "stats";
	public static final String ACTION_USER_REPUTATION = "user";
}
